<?php
    header('Access-Control-Allow-Origin:*');
	header('Access-Control-Allow-Method: PUT,GET, POST');
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
	$data = json_decode(file_get_contents("php://input"));
	$dbUserName = 'root';
	$dbPassword = '';
	$host = 'localhost';
	$dbName = 'test';
	$conn = mysqli_connect($host, $dbUserName, $dbPassword, $dbName);
	if(!$conn) {
		echo "Could not connect to database".mysqli_error($conn);
		exit;
	}
	$query = mysqli_query($conn, "SELECT * FROM country");
	$result = [];
	while($row = mysqli_fetch_assoc($query)) {
		$result[] = $row;
	}
	echo json_encode($result);
	
?>