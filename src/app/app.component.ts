import { Component } from '@angular/core';
import { AuthComponent } from './auth/auth.component';
// import { AuthService } from './service/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	auth;
	data;
	iD : number;
	constructor(private router: Router,private route: ActivatedRoute) {}
	ngOnInit() {
		console.log(1);
		this.auth = new AuthComponent();
		this.data = this.auth.authRedirect();
		if(this.data!=null && this.data.reg_id > 0) {
			this.iD = this.data.reg_id;
			this.router.navigate(['/userList']);
		} else {
			this.iD = 0;
			this.router.navigate(['/login']);
		}
	}
	logOut() {
		if(this.data.reg_id > 0) {
			localStorage.clear();
			this.router.navigate(['/login']);
			this.iD = 0;
		}
	}
}
