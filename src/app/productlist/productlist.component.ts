import { Component, OnInit } from '@angular/core';
import { Http , Response} from '@angular/http';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {

  constructor(private http:Http) { }

  ngOnInit() {
	  this.getProductsInfo();
  }
  public function getProductsInfo() {
	  this.http.get("http://localhost/angular-php/productlist.php").subscribe((response: Response) => {
		    this.productResult = response.json();
		    console.log(response.json());
	   });

}
