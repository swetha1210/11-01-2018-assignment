import { Component, OnInit } from '@angular/core';
import { Http , Response, Headers, RequestOptions} from '@angular/http';
import { CategoryService } from '../service/category.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  categoryObj = [];
  data = {};
  fileData : any;
  formDataArray = [];
  constructor(private http:Http, private category:CategoryService) { }

  ngOnInit() {
	 this.getCategory();
  }
   public fileUploader(event) {
          const elem = event.target;
          if (elem.files.length > 0) {
			  this.fileData = elem.files[0];
			  let formData = new FormData();
			  formData.append('file',elem.files[0]);
			  this.http.post('http://localhost/angular-php/addProduct.php',formData)
	          .subscribe((data) => {
			 console.log(data)
	     }, (error) => {
		     console.error('Error:'  + error );
		 });
			  console.log(formData);
		  }
   }
  private getCategory(){
	  this.category.getCategoryList().subscribe((response: Response) => {
		    this.categoryObj = response.json();
		    //console.log(response.json());
	   });
  }
  private addProduct(productData) {
	productData.imageName = this.fileData.name;
	  const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });
	  this.http.post('http://localhost/angular-php/addProduct.php',productData, options)
	   .subscribe((data) => {
			 console.log(data)
	     }, (error) => {
		     console.error('Error:'  + error );
		 });
  }
}
