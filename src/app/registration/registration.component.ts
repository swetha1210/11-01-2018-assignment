import { Component, OnInit } from '@angular/core';
import { Http , Response, Headers, RequestOptions} from '@angular/http';
import { CountryService } from '../service/country.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {

  constructor(private http:Http, private countryservice:CountryService) {
  }

  userDetailsObj = {};
  data = {};
  countryObj = [];
   ngOnInit() {
	    this.countryList();
   }
   onFormSubmit(details) {
	   this.userDetailsObj = {
	      'fname' : details.fname,
		  'lname' : details.lname,
		  'email' : details.email,
		  'pwd'   : details.pwd
	   }

	   const headers = new Headers({ 'Content-Type': 'application/json' });
       const options = new RequestOptions({ headers: headers });

	   this.http.post('http://localhost/angular-php/hello.php',JSON.stringify(details), options)
	   .subscribe((data) => {
			 console.log(data)
	     }, (error) => {
		     console.error('Error:'  + error );
		 });
   }

   countryList = function() {
	   this.countryservice.getCountryList().subscribe((response: Response) => {
		    this.countryObj = response.json();
			//console.log(this.countryObj);
	   });
   }

}
