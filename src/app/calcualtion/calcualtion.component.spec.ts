import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcualtionComponent } from './calcualtion.component';

describe('CalcualtionComponent', () => {
  let component: CalcualtionComponent;
  let fixture: ComponentFixture<CalcualtionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcualtionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcualtionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
