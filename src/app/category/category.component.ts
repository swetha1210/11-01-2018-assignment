import { Component, OnInit } from '@angular/core';
import { Http , Response, Headers, RequestOptions} from '@angular/http';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  data;
  constructor(private http:Http) { }

  ngOnInit() {
  }
  private addCategory(cData) {
	  const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });
	  this.http.post('http://localhost/angular-php/addCategory.php',JSON.stringify(cData), options).subscribe((data) => {
			 console.log(data)
	     }, (error) => {
		     console.error('Error:'  + error );
		 });
  }
}
