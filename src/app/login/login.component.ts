import { Component, OnInit} from '@angular/core';
import { AuthComponent } from '../auth/auth.component';
import { Http,  Response, Headers, RequestOptions} from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


@Component({
	selector:'login',
	templateUrl:'./login.component.html',
	styleUrls : ['./login.component.css']
})

export class LoginComponent implements OnInit {
	result = '';
	message = '';
	auth = '';
	data;
	iD : number;
	constructor(private router: Router,private route: ActivatedRoute, private http : Http) {}
	ngOnInit() {
		this.auth = new AuthComponent();
		this.data = this.auth.authRedirect();
		if(this.data!=null && this.data.reg_id > 0 && this.data.reg_id!='undefined' && this.data.reg_id!=null) {
			this.iD = this.data.reg_id;
			this.router.navigate(['/userList']);
		} else {
			this.iD = 0;
			this.router.navigate(['/login']);
		}
	}
	login(details) {
	  const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });
	  this.http.post('http://localhost/angular-php/login.php', JSON.stringify(details), options).subscribe((data) => {
		 this.result = data.json();
		   //console.log(this.result['message']);
		   if(this.result['message'] == 0 && this.result['message']!='undefined') {
			   this.message = "Invalid user name and password";
		   } else {
			   localStorage.setItem('currentUser',JSON.stringify(this.result[0]));
			   this.router.navigate(['/userList']);
		   }
	     }, (error) => {
		     console.error('Error:'  + error );
		 });
  }
}