import { Component, OnInit } from '@angular/core';
// import { Http,  Response, Headers, RequestOptions} from '@angular/http';
// import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
// import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  iD : number;
  userDetails = '';
  constructor(private router: Router) {}
  ngOnInit() {}
  getLocalStorage () {
	  return localStorage.getItem('currentUser');
  }
  authRedirect() {
	  this.iD = 0;
	  this.userDetails = JSON.parse(this.getLocalStorage());
	  if(this.userDetails!=null && this.userDetails.reg_id > 0 && this.userDetails.reg_id!='undefined') {
		 return this.userDetails;
	  } else {
		   return this.userDetails;
	  }
  }
}
