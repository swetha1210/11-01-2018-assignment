import { Component, OnInit } from '@angular/core';
import { Http,  Response, Headers, RequestOptions} from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  id: number;
  fname: string;
  lName: string;
  userEmail : string
  data : {}
  constructor(private router: Router,private route: ActivatedRoute, private http : Http) { }

  ngOnInit() {
	  this.route.params.subscribe(params => {
		  this.id = +params['id'];
	  });
	  this.http.get('http://localhost/angular-php/getUserDetail.php?id='+this.id).subscribe((response: Response) => {
		   this.data = response.json();
		   this.fname = this.data[0].fname;
		   this.lName =  this.data[0].lname;
		   this.userEmail = this.data[0].email;
	   });
  }
  
  editUserDetail(details) {
	  const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });
	  let userID = {'id':this.id};
	  let params = {details,userID};
	  this.http.post('http://localhost/angular-php/editUserDetail.php', JSON.stringify(params), options).subscribe((data) => {
			 this.router.navigate(['/userList']);
	     }, (error) => {
		     console.error('Error:'  + error );
		 });
  }
}
