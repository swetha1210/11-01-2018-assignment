import { Component, OnInit } from '@angular/core';
import { AuthComponent } from '../auth/auth.component';
import { Http, Response } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {
	iD : number;
	auth = '';
	userDetails = '';
   constructor(private http:Http,private router: Router,private route: ActivatedRoute) {}
  data : {}
  ngOnInit() {
	  this.auth = new AuthComponent();
	  this.auth.authRedirect();
	  this.userDetails = this.auth.authRedirect();
		if(this.userDetails.reg_id > 0) {
			this.iD = this.userDetails.reg_id;
			this.router.navigate(['/userList']);
		} else {
			this.iD = 0;
			this.router.navigate(['/login']);
		}
	  console.log(this.auth.authRedirect());
	  this.http.get('http://localhost/angular-php/getDetails.php').subscribe((response: Response) => {
		  this.data = response.json();
	   });
  }
}
