import { Injectable } from '@angular/core';
import { Http,Request } from '@angular/http'

@Injectable()

export class CategoryService {
	constructor(private http: Http){ }
	getCategoryList() {
		return this.http.get('http://localhost/angular-php/getCategory.php');
	}
}