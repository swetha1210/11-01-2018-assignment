import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NewCmpComponent } from './new-cmp/new-cmp.component';
import { CalcualtionComponent } from './calcualtion/calcualtion.component';
import { RegistrationComponent } from './registration/registration.component';
import { UpdateComponent } from './update/update.component';
import { UserlistComponent } from './userlist/userlist.component';
import { LoginComponent } from './login/login.component';
import { CountryService } from './service/country.service';
// import { HeaderComponent } from './header/header.component';
import { DasbaordComponent } from './dasbaord/dasbaord.component';
import { AuthComponent } from './auth/auth.component';
import { ProductComponent } from './product/product.component';
import { CategoryComponent } from './category/category.component';
import { AuthService } from './service/auth.service';
import { CategoryService } from './service/category.service';
import { ProductlistComponent } from './productlist/productlist.component';

@NgModule({
  declarations: [
    AppComponent,
    NewCmpComponent,
    CalcualtionComponent,
    RegistrationComponent,
    UpdateComponent,
    UserlistComponent,
	LoginComponent,
	// HeaderComponent,
	DasbaordComponent,
	AuthComponent,
	ProductComponent,
	CategoryComponent,
	ProductlistComponent
  ],
  imports: [
    BrowserModule,
	FormsModule,
	ReactiveFormsModule,
	HttpModule,
	RouterModule.forRoot([
	{path:'', component: DasbaordComponent},
	{path:'addcategory', component:CategoryComponent},
	{path:'login', component:LoginComponent},
	{path:'addproduct', component:ProductComponent},
	{path:'productLsit', component:ProductlistComponent},
	{path:'signup', component: RegistrationComponent},
	{path:'userList',component: UserlistComponent},
	{path:'update/:id',component: UpdateComponent},
	])
   ],
  providers: [CountryService, AuthService, CategoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }

