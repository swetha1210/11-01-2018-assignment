import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DasbaordComponent } from './dasbaord.component';

describe('DasbaordComponent', () => {
  let component: DasbaordComponent;
  let fixture: ComponentFixture<DasbaordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DasbaordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DasbaordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
